package com.example.sarthakjetly.newassignment6.main;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.sarthakjetly.newassignment6.R;
import com.example.sarthakjetly.newassignment6.entities.Activity2;
import com.example.sarthakjetly.newassignment6.entities.DbController;
import com.example.sarthakjetly.newassignment6.entities.Student;
import com.example.sarthakjetly.newassignment6.entities.TestAdapter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    int roll_no;
    TestAdapter adapter;
    ListView listView;
    DbController dbController = new DbController(MainActivity.this);
    boolean caseEdit;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        List<Student> list = new ArrayList<Student>();
        DbController db = new DbController(MainActivity.this);
        db.open();
        list = db.getData();
        db.close();
        adapter = new TestAdapter(this, list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {

                LayoutInflater li = LayoutInflater.from(MainActivity.this);
                view = li.inflate(R.layout.phase2, null);
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).create();
                dialog.setView(view);
                dialog.setTitle("phase2");


                view.findViewById(R.id.Delete).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        String name = adapter.list.get(position).getName();
                        int roll = adapter.list.get(position).getRoll_no();


                        adapter.list.remove(position);
                        adapter.notifyDataSetChanged();
dialog.dismiss();
                    }
                });


                view.findViewById(R.id.Edit).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view)

                    {
                        String name1 = adapter.list.get(position).name;
                        int roll = adapter.list.get(position).roll_no;
                        Bundle b = new Bundle();
                        Intent i = new Intent(MainActivity.this, Activity2.class);
                        b.putString("name", name1);
                        b.putInt("roll_no", roll);
                        i.putExtras(b);
                        startActivityForResult(i, 10);
                        adapter.list.remove(position);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void newStart() {
        Intent intent = new Intent(this, Activity2.class);
        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

                String name = data.getStringExtra("name");
                int roll_no = data.getIntExtra("roll_no", 0);
                adapter.list.add(new Student(name, roll_no));
                adapter.notifyDataSetChanged();
            }
        }
        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
                String name = extras.getString("name");
                int roll_no = extras.getInt("roll_no", 0);
                adapter.list.add(new Student(name, roll_no));
                adapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bttnId:
                newStart();
                break;
            default:
                break;

        }
    }

    public class loadSomeStuff extends AsyncTask<String, Integer, String> {
        int roll_no;

        public loadSomeStuff(int roll_no) {
            this.roll_no = roll_no;
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }


        protected String doInBackground(String... params) {
            if (params[0].equals("delete")) {


                dbController.open();
                try {
                    long l = Long.parseLong(String.valueOf(roll_no));
                    dbController.deleteEntry(l);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {

                Student stu = dbController.getSingleRow(roll_no);
                String name = stu.getName();
                int roll = stu.getRoll_no();
                Intent intent = new Intent(MainActivity.this, Activity2.class);
                intent.putExtra("name", name);
                intent.putExtra("roll_no", roll);
                startActivity(intent);
            }
            dbController.close();


            return null;
        }

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}







