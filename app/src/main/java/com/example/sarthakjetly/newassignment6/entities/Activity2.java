package com.example.sarthakjetly.newassignment6.entities;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sarthakjetly.newassignment6.R;

public class Activity2 extends ActionBarActivity {

    int roll_no;
    String name;
    EditText editText1;
    EditText editText2;
    DbController dbController;
    boolean caseStart;

    Intent intent = new Intent();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        editText1 = (EditText) findViewById(R.id.editName);
        editText2 = (EditText) findViewById(R.id.editRoll);
        Bundle receiveBundle = this.getIntent().getExtras();
        if (receiveBundle != null) {
            caseStart = true;
            final String receive1 = receiveBundle.getString("name");
            editText1.setText(String.valueOf(receive1));
            final int receive2 = receiveBundle.getInt("roll_no");
            editText2.setText(Integer.toString(receive2));
        }
        Button btnSave = (Button) findViewById(R.id.Save);
        Button btnCancel = (Button) findViewById(R.id.Cancel);
        String name = intent.getStringExtra("name");
        int roll = intent.getIntExtra("roll_no", 0);
        if (name != null && (roll != 0)) {
            btnSave.setEnabled(false);
            btnCancel.setEnabled(false);

            editText1.setText(name);
            editText2.setText(String.valueOf(roll));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }

    public void onClick(View view)

    {
        switch (view.getId()) {

            case R.id.Save:
                name = editText1.getText().toString();
                roll_no = Integer.parseInt(editText2.getText().toString());
                new loadSomeStuff(name, roll_no).execute(caseStart ? "edit" : "save");
                Intent intent = new Intent();
                intent.putExtra("name", name);
                intent.putExtra("roll_no", roll_no);
                setResult(RESULT_OK, intent);
                finish();
            case R.id.Cancel:
                setResult(RESULT_CANCELED);
                finish();
        }
    }

    public class loadSomeStuff extends AsyncTask<String, Integer, String> {
        String name;
        int roll_no;

        public loadSomeStuff(String name, int roll_no) {
            this.name = name;
            this.roll_no = roll_no;
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

        }

        protected String doInBackground(String... params) {

            dbController = new DbController(Activity2.this);
            dbController.open();
            long l = Long.parseLong(String.valueOf(roll_no));
            if (params[0].equals("save")) {
                dbController.createEntry(name, roll_no);
            } else {
                dbController.updateEntry(name, roll_no);
            }
            dbController.close();
            return null;
        }

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}






