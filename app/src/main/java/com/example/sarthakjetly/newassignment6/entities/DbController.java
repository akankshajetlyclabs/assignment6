package com.example.sarthakjetly.newassignment6.entities;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sarthakjetly.newassignment6.util.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DbController implements Constants {
    public DbHelper ourHelper;
    Context ourContext;
    int iName;
    int iRoll;

    private SQLiteDatabase ourDatabase;

    public DbController(Context con) {
        ourContext = con;
    }

    public DbController open() {
        ourHelper = new DbHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        ourHelper.close();
    }

    public long createEntry(String name, int roll_no) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_ROLLNO, roll_no);

        return ourDatabase.insert(DATABASE_TABLE, null, contentValues);
    }

    public Student getSingleRow(int roll_no) {

       Cursor cursor = ourDatabase.query(DATABASE_TABLE, new String[] { KEY_ROLLNO,KEY_NAME },KEY_ROLLNO + "=?", new String[] { String.valueOf(roll_no) },null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        Student stu = new Student(cursor.getString(0), Integer.valueOf(cursor.getString(1)));

        return stu;


    }

    public void updateEntry(String name, int roll_no) {
        ContentValues cvUpdate = new ContentValues();
        cvUpdate.put(KEY_NAME, name);
        cvUpdate.put(String.valueOf(KEY_ROLLNO), roll_no);

        ourDatabase.update(DATABASE_TABLE, cvUpdate, KEY_ROLLNO + "=" + roll_no, null);
    }

    public void deleteEntry(long roll_no) throws SQLException {
        ourDatabase.delete(DATABASE_TABLE, KEY_ROLLNO + "=" + roll_no, null);

    }

    public List<Student> getData() {
        List<Student> list = new ArrayList<>();
        String[] columns = new String[]{KEY_ROLLNO, KEY_NAME};
        Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String result = "";

        int iName = c.getColumnIndex(KEY_NAME);
        int iRoll = c.getColumnIndex(KEY_ROLLNO);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            list.add(new Student(c.getString(iName), Integer.parseInt(c.getString(iRoll))));

        }
        return list;
    }
}







