package com.example.sarthakjetly.newassignment6.entities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sarthakjetly.newassignment6.util.Constants;


public class DbHelper extends SQLiteOpenHelper implements Constants {

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" + KEY_ROLLNO
                + " INTEGER PRIMARY KEY AUTOINCREMENT , " + KEY_NAME + " TEXT NOT NULL);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);

    }

}