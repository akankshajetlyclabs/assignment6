package com.example.sarthakjetly.newassignment6.entities;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sarthakjetly.newassignment6.R;

import java.util.List;

public class TestAdapter extends BaseAdapter {
    public List<Student> list;
    Context context;

    public TestAdapter(Context context, List<Student> list) {
        this.context = context;
        this.list = list;
    }

    public int getCount() {
        return list.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_items, null);
        TextView textView = (TextView) view.findViewById(R.id.detail1);
        textView.setText(list.get(position).name);
        TextView textView1 = (TextView) view.findViewById(R.id.detail2);
        textView1.setText(list.get(position).roll_no + "");
        return view;
    }

    public long getItemId(int position) {
        return 0;
    }

    public Object getItem(int position) {
        return list.get(position);
    }


}


