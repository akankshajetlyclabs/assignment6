package com.example.sarthakjetly.newassignment6.util;


public interface Constants {
    public static final String KEY_NAME = "name";
    public static final String KEY_ROLLNO = "roll_no";
    public static final String DATABASE_NAME = "students_detail";
    public static final String DATABASE_TABLE = "student_table";
    public static final int DATABASE_VERSION = 1;
}
